<?php namespace Gameplan\LibPhoneNumberLaravel;

use Illuminate\Support\ServiceProvider;
use libphonenumber\PhoneNumberUtil;

class LibPhoneNumberLaravelServiceProvider extends ServiceProvider {

	/**
	 * Boot the service provider.
	 *
	 * @return void
	 */
	public function boot()
	{

	}

	/**
	 * Register the service provider.
	 *
	 * @return void
	 */
	public function register()
	{
		$this->app->singleton('libphonenumber', function ($app)
		{
			return PhoneNumberUtil::getInstance();
		});

		$this->app->alias('libphonenumber', 'libphonenumber\PhoneNumberUtil');
	}

	 /**
     * Get the services provided by the provider.
     *
	 * @return array
	 */
	public function provides()
	{
		return ['libphonenumber','libphonenumber\PhoneNumberUtil'];
	}
}