# libphonenumber-laravel

This is a Service Provider and Facade package for the PHP library libphonenumber-for-php to make using the package with Laravel really easy.

## Composer

To install libphonenumber-laravel as a Composer package, simply add this to your composer.json:

```json
"gameplan/libphonenumber-laravel": "1.1"
```

..and run `composer update`.  Once it's installed, you can register the service provider in `app/config/app.php` in the `providers` array add :

```php
'GamePlan\LibPhoneNumberLaravel\LibPhoneNumberLaravelServiceProvider',
```

You can also benefit from using a Facade in Laravel 5 by adding to the alias array also in app.php below the providers array

```php
'LibPhoneNumber' => 'GamePlan\LibPhoneNumberLaravel\LibPhoneNumberLaravelFacade',
```